import { Size } from "./size";
import Tags from "./tags";

export default class MetaData {
  public tags: Tags;
  public size: Size;

  get hasTags(): boolean {
    return this.tags !== null;
  }

  get hasSize(): boolean {
    return this.size !== null;
  }
}
