export default class Urls {
  public raw: string;
  public full: string;
  public regular: string;
  public small: string;
  public thumb: string;
}
