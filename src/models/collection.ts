import CollectionLinks from "./collectionLinks";
import User from "./user";

export default class Collection {
  public id: number;
  public title: string;
  public description: string;
  public totalPhotos: number;
  public shareKey: string;
  public user: User;
  public links: CollectionLinks;
}
