export default class Tags {
  private _separator: string;
  get separator() {
    return this._separator;
  }
  set separator(value) {
    this._separator = value;
  }

  private _values: string[];
  get values() {
    return this._values;
  }

  constructor(separator: string = " ") {
    this._values = new Array<string>();
    this.separator = separator;
  }

  public combine(): string {
    return this.values.join(this.separator);
  }
}
