import Photo from "./photo";

export default class CollectionCache {
  private _photos: Map<string, Photo>;
  get photos() {
    return this._photos;
  }

  private _timeStamp: number;
  get timeStamp() {
    return this._timeStamp;
  }

  constructor(photos?: Map<string, Photo>, timeStamp?: number) {
    photos ? (this._photos = photos) : this.reset();
    timeStamp ? (this._timeStamp = timeStamp) : this.updateTimeStamp();
  }

  public updateTimeStamp() {
    this._timeStamp = Date.now();
  }

  public reset() {
    this._photos = new Map<string, Photo>();
  }
}
