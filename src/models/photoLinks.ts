export default class PhotoLinks {
  public self: string;
  public html: string;
  public download: string;
  public downloadLocation: string;
}
