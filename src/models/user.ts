export default class User {
  public id: number;
  public username: string;
  public name: string;
  public portfolioUrl: string;
  public bio: string;
  public location: string;
}
