export default class Options {
  public width: number;
  public height: number;
  public orientation: string;
  public username: string;
  public featured: boolean;
  public collections: string[];
  public query: string;
  public count: number;

  public get hasCount(): boolean {
    return this.count !== undefined && this.count !== null;
  }
}
