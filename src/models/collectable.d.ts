import Photo from "./photo";
import CollectionCache from "./collectionCache";

export default abstract class Collectable {
  name: string;
  path: string;

  collection: Map<string, Photo>;
}
