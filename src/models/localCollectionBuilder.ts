import CollectionCache from "./collectionCache";
import LocalCollection from "./localCollection";

export default class LocalCollectionBuilder {
  private collection: LocalCollection;

  constructor() {
    this.reset();
  }

  public setName(value: string) {
    this.collection.name = value;
  }

  public setPath(value: string) {
    this.collection.path = value;
  }

  public setCache(value: CollectionCache) {
    this.collection.cache = value;
  }

  public reset() {
    this.collection = new LocalCollection();
  }

  public build(): LocalCollection {
    const collection = this.collection;

    this.reset();

    return collection;
  }
}
