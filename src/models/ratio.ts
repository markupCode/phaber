import InvalidArgumentError from "../exceptions/invalidArgumentError";

export class Ratio {
  private _type: RatioType;
  public get type() {
    return this._type;
  }

  private _width: number;
  get width() {
    return this._width;
  }
  set width(value: number) {
    this.checkRatioValue(value);
    this._width = value;
    this.updateAspect();
  }

  private _height: number;
  get height() {
    return this.height;
  }
  set height(value: number) {
    this.checkRatioValue(value);
    this._height = value;
    this.updateAspect();
  }

  constructor(width: number = 1, height: number = 1) {
    this.update(width, height);
  }

  public update(width: number, height: number) {
    this.checkRatioValue(width);
    this.checkRatioValue(height);

    this._width = width;
    this._height = height;

    this.updateAspect();
  }

  private updateAspect() {
    const devider = gcd(this.width, this.height);

    this._width /= devider;
    this._height /= devider;

    this.updateType();
  }

  private updateType() {
    let type = RatioType.LandScape;
    const coefficient = this.coefficient;

    if (Math.abs(coefficient - 1) <= 0.2) type = RatioType.Squarish;
    else if (coefficient < 0.8) type = RatioType.Portrait;

    this._type = type;
  }

  /**
   * represent width-to-height ratio
   */
  private get coefficient(): number {
    return this.width / this.height;
  }

  private checkRatioValue(value: number) {
    if (value <= 0)
      throw new InvalidArgumentError(
        value,
        "ratio should be greater than zero"
      );
  }
}

export enum RatioType {
  LandScape = "landscape",
  Portrait = "portrait",
  Squarish = "squarish"
}

/**
 * Greatest Common Divisor uses the Euclidean Algorithm
 */
function gcd(a: number, b: number): number {
  if (b === 0) return a;
  return gcd(b, a % b);
}
