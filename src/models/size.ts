import InvalidArgumentError from "../exceptions/invalidArgumentError";
import { Ratio } from "./ratio";

export class Size {
  private _width: number;
  get width() {
    return this._width;
  }
  set width(value) {
    this.checkDimension(value);
    this._width = value;
    this.updateRatio();
  }

  private _height: number;
  get height() {
    return this._height;
  }
  set height(value) {
    this.checkDimension(value);
    this._height = value;
    this.updateRatio();
  }

  private _ratio: Ratio;
  get ratio(): Ratio {
    return this._ratio;
  }

  constructor(width: number = 1, height: number = 1) {
    this.update(width, height);
  }

  public update(width: number, height: number) {
    this.checkDimension(width);
    this.checkDimension(height);

    this._width = width;
    this._height = height;

    this.updateRatio();
  }

  private updateRatio() {
    this._ratio = new Ratio(this.width, this.height);
  }

  private checkDimension(value: number) {
    if (value <= 0)
      throw new InvalidArgumentError(value, "size should be greater than zero");
  }
}
