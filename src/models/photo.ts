import PhotoLinks from "./photoLinks";
import Urls from "./urls";
import User from "./user";

export default class Photo {
  public id: number;
  public width: number;
  public height: number;
  public color: string;
  public description: string;
  public user: User;
  public urls: Urls;
  public links: PhotoLinks;
}
