import Collection from "../models/collection";
import CollectionLinks from "../models/collectionLinks";
import Photo from "../models/photo";
import PhotoLinks from "../models/photoLinks";
import Urls from "../models/urls";
import User from "../models/user";
import AdapterFactory from "./adapterFactory";
import JsonToCollectionAdapter from "./jsonToCollectionAdapter";
import JsonToPhotoAdapter from "./jsonToPhotoAdapter";
import JsonToPhotoLinksAdapter from "./jsonToPhotoLinksAdapter";
import JsonToUserAdapter from "./jsonToUserAdapter";

export default class JsonAdapterFactory implements AdapterFactory {
  public getPhotoAdapter(source: any): Photo {
    return new JsonToPhotoAdapter(
      source,
      this.getUserAdapter(source.user),
      this.getPhotoLinksAdapter(source.links)
    );
  }

  public getCollectionAdapter(source: any): Collection {
    return new JsonToCollectionAdapter(
      source,
      this.getUserAdapter(source.user)
    );
  }

  public getUserAdapter(source: any): User {
    return new JsonToUserAdapter(source);
  }

  public getPhotoLinksAdapter(source: any): PhotoLinks {
    return new JsonToPhotoLinksAdapter(source);
  }
}
