import User from "../models/user";

/**
 * represent mapping between`Unsplash.User`
 * from **Unsplash-JS** and core `User` class
 */
export default class JsonToUserAdapter implements User {
  get id(): number {
    return this.source.id;
  }
  set id(value) {
    this.source.id = value;
  }

  get username(): string {
    return this.source.username;
  }
  set username(value) {
    this.source.username = value;
  }

  get name(): string {
    return this.source.name;
  }
  set name(value) {
    this.source.name = value;
  }

  get portfolioUrl(): string {
    return this.source.portfolio_url;
  }
  set portfolioUrl(value) {
    this.source.portfolio_url = value;
  }

  get bio(): string {
    return this.source.bio;
  }
  set bio(value) {
    this.source.bio = value;
  }

  get location(): string {
    return this.source.location;
  }
  set location(value) {
    this.source.location = value;
  }

  protected source: any;

  constructor(source: any) {
    this.source = source;
  }
}
