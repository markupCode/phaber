import Photo from "../models/photo";
import PhotoLinks from "../models/photoLinks";
import Urls from "../models/urls";
import User from "../models/user";

/**
 * represent mapping between`Unsplash.Photo`
 * from **Unsplash-JS** and core `Photo` class
 */
export default class JsonToPhotoAdapter implements Photo {
  get id(): number {
    return this.source.id;
  }
  set id(value) {
    this.source.id = value;
  }

  get width(): number {
    return this.source.width;
  }
  set width(value) {
    this.source.width = value;
  }

  get height(): number {
    return this.source.height;
  }
  set height(value) {
    this.source.height = value;
  }

  get color(): string {
    return this.source.color;
  }
  set color(value) {
    this.source.color = value;
  }

  get description(): string {
    return this.source.description;
  }
  set description(value) {
    this.source.description = value;
  }

  get user(): User {
    return this._user;
  }
  set user(value) {
    this._user = value;
  }

  get urls(): Urls {
    return this.source.urls;
  }
  set urls(value) {
    this.source.urls = value;
  }

  get links(): PhotoLinks {
    return this._links;
  }
  set links(value) {
    this._links = value;
  }

  protected source: any;

  private _user: User;
  private _links: PhotoLinks;

  constructor(source: any, user: User, links: PhotoLinks) {
    this.source = source;
    this.setInnerAdapters(user, links);
  }

  protected setInnerAdapters(user: User, links: PhotoLinks) {
    this._user = user;
    this._links = links;
  }
}
