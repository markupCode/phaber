import SearchApi from "./searchApi";

export default interface ApiFabric {
  getSearchApi(): SearchApi;
}
