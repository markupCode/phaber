import Unsplash from "unsplash-js";
import AdapterFactory from "../../adapters/adapterFactory";

import PaginationStrategies, {
  PaginationStrategy
} from "../pagination/paginationStrategies";

import JsonAdapterFactory from "../../adapters/jsonAdapterFactory";
import ApiFabric from "./apiFabric";
import SearchApi from "./searchApi";
import UnsplashSearchApi from "./unsplashSearchApi";

export default class UnsplashApiFabric implements ApiFabric {
  private client: Unsplash;
  private adapters: AdapterFactory;
  private strategies: PaginationStrategies;

  constructor(client: Unsplash) {
    this.client = client;
    this.adapters = new JsonAdapterFactory();
    this.strategies = new PaginationStrategies();
  }

  public getSearchApi(): SearchApi {
    return new UnsplashSearchApi(
      this.client,
      this.adapters,
      this.strategies.getItem(PaginationStrategy.PRODUCTION)
    );
  }
}
