import Collection from "../../models/collection";

export default interface SearchApi {
  byId(id: number): Promise<Collection>;
  byName(name: string, count: number): Promise<Collection[]>;
}
