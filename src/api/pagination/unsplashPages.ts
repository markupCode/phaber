import Page from "./page";
import Pages from "./pages";
import PagesIterator from "./pagesIterator";
import PaginationOptions from "./paginationOptions";

export default class UnsplashPages implements Pages {
  private _count: number;
  get count(): number {
    return this._count;
  }

  private _perPage: number;
  get perPage(): number {
    return this._perPage;
  }

  constructor(count: number, options: PaginationOptions) {
    this._perPage = options.MAX_PER_PAGE;
    this._count = Math.ceil(count / options.MAX_PER_PAGE);
  }

  get iterator(): IterableIterator<Page> {
    return new PagesIterator(this);
  }
}
