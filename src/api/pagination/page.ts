export default class Page {
  private _number: number;
  get number(): number {
    return this._number;
  }

  constructor(pageNumber: number) {
    this._number = pageNumber;
  }
}
