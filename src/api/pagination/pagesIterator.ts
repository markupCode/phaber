import Page from "./page";
import Pages from "./pages";

export default class PagesIterator implements IterableIterator<Page> {
  private pages: Pages;
  private current: Page;

  constructor(pages: Pages) {
    this.pages = pages;
    this.initZeroPage();
  }

  public next(): IteratorResult<Page> {
    if (this.hasNext()) {
      this.current = new Page(this.current.number + 1);

      return {
        done: false,
        value: this.current
      };
    }

    return { done: true, value: null };
  }

  public hasNext(): boolean {
    return this.pages.count > this.current.number;
  }

  public [Symbol.iterator](): IterableIterator<Page> {
    return this;
  }

  protected initZeroPage(page: number = 0) {
    this.current = new Page(page);
  }
}
