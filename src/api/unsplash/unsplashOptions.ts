export default class UnsplashOptions {
  public apiUrl: string;
  public apiVersion: string;
  public applicationId: string;
  public secret: string;
  public callbackUrl: string;
  public bearerToken: string;
  public headers: object;
}
