import Unsplash from "unsplash-js";
import UnsplashOptions from "./unsplashOptions";

import "cross-fetch/polyfill";

export default class UnsplashBuilder {
  private options: UnsplashOptions;

  constructor() {
    this.reset();
  }

  public setApiUrl(value: string) {
    this.options.apiUrl = value;
  }

  public setApiVersion(value: string) {
    this.options.apiVersion = value;
  }

  public setApplicationId(value: string) {
    this.options.applicationId = value;
  }

  public setSecret(value: string) {
    this.options.secret = value;
  }

  public setCallbackUrl(value: string) {
    this.options.callbackUrl = value;
  }

  public setBearerToken(value: string) {
    this.options.bearerToken = value;
  }

  public setHeader(value: object) {
    this.options.headers = value;
  }

  public reset() {
    this.options = new UnsplashOptions();
  }

  public build(): Unsplash {
    const client = new Unsplash(this.options);

    this.reset();

    return client;
  }
}
