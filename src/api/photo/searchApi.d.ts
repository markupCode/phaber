import Options from "../../models/options";
import Collection from "../../models/collection";
import Photo from "../../models/photo";

export default interface SearhApi {
  byId(id: string): Promise<Photo>;
  byName(name: string, count?: number): Promise<Photo[]>;
  byOptions(options: Options): Promise<Photo[]>;
  byCollection(collection: Collection): Promise<Photo[]>;
}
