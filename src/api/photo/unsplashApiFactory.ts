import Unsplash from "unsplash-js";
import AdapterFactory from "../../adapters/adapterFactory";
import MapperFactory from "../../mappers/mapperFactory";

import PaginationStrategies, {
  PaginationStrategy
} from "../pagination/paginationStrategies";

import JsonAdapterFactory from "../../adapters/jsonAdapterFactory";
import UnsplashMapperFactory from "../../mappers/UnsplashMapperFactory";
import ApiFactory from "./apiFactory";
import DownloadApi from "./downloadApi";
import SearchApi from "./searchApi";
import UnsplashDownloadApi from "./unsplashDownloadApi";
import UnsplashSearchApi from "./unsplashSearchApi";

export default class UnsplashApiFactory implements ApiFactory {
  private client: Unsplash;
  private adapters: AdapterFactory;
  private mappers: MapperFactory;
  private strategies: PaginationStrategies;

  constructor(client: Unsplash) {
    this.client = client;
    this.adapters = new JsonAdapterFactory();
    this.mappers = new UnsplashMapperFactory();
    this.strategies = new PaginationStrategies();
  }

  public getSearchApi(): SearchApi {
    return new UnsplashSearchApi(
      this.client,
      this.adapters,
      this.strategies.getItem(PaginationStrategy.PRODUCTION)
    );
  }

  public getDownloadApi(): DownloadApi {
    return new UnsplashDownloadApi(this.client, this.mappers);
  }
}
