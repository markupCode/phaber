import Unsplash, { toJson } from "unsplash-js";
import AdapterFactory from "../../adapters/adapterFactory";
import Collection from "../../models/collection";
import Options from "../../models/options";
import Photo from "../../models/photo";
import PaginationOptions from "../pagination/paginationOptions";
import UnsplashPages from "../pagination/unsplashPages";
import SearchApi from "./searchApi";

export default class UnsplashSearchApi implements SearchApi {
  private client: Unsplash;
  private adapters: AdapterFactory;
  private options: PaginationOptions;

  constructor(
    client: Unsplash,
    adapters: AdapterFactory,
    options: PaginationOptions
  ) {
    this.client = client;
    this.adapters = adapters;
    this.options = options;
  }

  public async byId(id: string): Promise<Photo> {
    const json = await toJson(await this.client.photos.getPhoto(id));

    return this.adapters.getPhotoAdapter(json);
  }

  public async byName(name: string, count: number = 10): Promise<Photo[]> {
    const search = this.client.search.photos;
    const photos = [];

    const availableCount = await this.getAvailableItems(name);
    const receivedCount = Math.min(count, availableCount);

    const pages = new UnsplashPages(receivedCount, this.options);

    for (const page of pages.iterator) {
      const json = await toJson(await search(name, page.number, pages.perPage));
      photos.push(...json.results);
    }

    return this.mapJsonToPhotos(photos.slice(0, receivedCount));
  }

  /**
   * can't download more than 30 photo
   */
  public async byOptions(options: Options): Promise<Photo[]> {
    let json = await toJson(await this.client.photos.getRandomPhoto(options));

    if (!options.hasCount) json = [json];

    return this.mapJsonToPhotos(json);
  }

  public async byCollection(collection: Collection): Promise<Photo[]> {
    const loader = this.client.collections.getCollectionPhotos;
    const photos = [];

    const pages = new UnsplashPages(collection.totalPhotos, this.options);

    for (const page of pages.iterator) {
      const json = await toJson(
        await loader(collection.id, page.number, pages.perPage)
      );
      photos.push(...json);
    }

    return this.mapJsonToPhotos(photos);
  }

  private async getAvailableItems(query: string): Promise<number> {
    const json = await toJson(await this.client.search.photos(query, 1, 0));
    return parseInt(json.total, 10);
  }

  private mapJsonToPhotos(json: any): Photo[] {
    const photos = new Array<Photo>();
    json.forEach(item => {
      photos.push(this.adapters.getPhotoAdapter(item));
    });

    return photos;
  }
}
