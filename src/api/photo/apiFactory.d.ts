import DownloadApi from "./downloadApi";
import SearchApi from "./searchApi";

export default interface ApiFactory {
  getSearchApi(): SearchApi;
  getDownloadApi(): DownloadApi;
}
