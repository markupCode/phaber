import Unsplash, { toJson } from "unsplash-js";
import MapperFactory from "../../mappers/mapperFactory";
import Photo from "../../models/photo";
import DonwloadApi from "./unsplashDownloadApi";

import { stream } from "got";
import { Duplex } from "stream";

export default class UnsplashDownloadApi implements DonwloadApi {
  private client: Unsplash;
  private mappers: MapperFactory;

  constructor(client: Unsplash, mappers: MapperFactory) {
    this.client = client;
    this.mappers = mappers;
  }

  public async getDataStream(photo: Photo): Promise<Duplex> {
    const requestData = this.mappers.mapPhotoToJson(photo);
    const json = toJson(await this.client.photos.downloadPhoto(requestData));

    return stream((await json).url);
  }
}
