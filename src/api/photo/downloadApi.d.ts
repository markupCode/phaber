import Photo from "../../models/photo";
import { Duplex } from "stream";

export default interface DownloadApi {
  getDataStream(photo: Photo): Promise<Duplex>;
}
